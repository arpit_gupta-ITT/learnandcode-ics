import logging
from constants import constants

class LoggerLibrary:

    def __init__(self):
        self.setLogger()
    
    def setLogger(self):
        logging.basicConfig(
            level = logging.INFO, 
            filename = constants.LOGGER_FILE_NAME, 
            format = "%(levelname)s - (%(asctime)s) - %(message)s",
            datefmt = "%d-%b-%y %H:%M:%S"
        )

    def getLoggerObj(self):
        return logging        