from dotenv import load_dotenv

class EnvLibrary:

    def __init__(self):
        self.loadEnv()
    
    def loadEnv(self):
        load_dotenv()