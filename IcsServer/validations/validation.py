from libraries.regexLibrary import RegexLibrary
from constants import constants

class Validation:
    
    def __init__(self):    
        self.regexLibraryObj = RegexLibrary()

    def isValidConnectionCommand(self, command):
        return bool(self.regexLibraryObj.getRegexObj().match(constants.CONNECTION_COMMAND_PATTERN, command))
        
    def isValidCommand(self, command):
        if (self.isValidGetCommand(command) or self.isValidSetCommand(command)):
            return True
        else:
            return False
    
    def isValidGetCommand(self, command):
        return bool(self.regexLibraryObj.getRegexObj().match(constants.GET_COMMAND_PATTERN, command))
            
    def isValidSetCommand(self, command):
        return bool(self.regexLibraryObj.getRegexObj().match(constants.SET_COMMAND_PATTERN, command))
    
    def getSize(self, value):
        return len(value.encode('utf-8'))

    def isValueAlphaNumeric(self, value):
        return bool(self.regexLibraryObj.getRegexObj().match(constants.KEY_VALUE_PATTERN, value))
    
    def isKeyValid(self, key):
        return (self.getSize(key) <= constants.KEY_MAX_SIZE_IN_BYTES) and (self.isValueAlphaNumeric(key))

    def isValueValid(self, value):
        return (self.getSize(value) <= constants.VALUE_MAX_SIZE_IN_BYTES) and (self.isValueAlphaNumeric(value))