from libraries.osLibrary import OsLibrary
from config.envHandler import EnvHandler
from controllers.IcsProtocol import IcsProtocol
from controllers.RequestController import RequestController
from controllers.ResponseController import ResponseController
from controllers.OutputController import OutputController
from validations.validation import Validation
from config.typeConversion import TypeConversion
from config.logger import Logger

osLibraryObj = OsLibrary()
envhandlerObj = EnvHandler()
icsProtocolObj = IcsProtocol()
requestControllerObj = RequestController()
responseControllerObj = ResponseController()
outputControllerObj = OutputController()
validationObj = Validation()
typeConversionObj = TypeConversion()
loggerObj = Logger()