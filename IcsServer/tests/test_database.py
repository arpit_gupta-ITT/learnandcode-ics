from controllers.databaseControllers.PersistentCacheDAOImpl import PersistentCacheDAOImpl

def test_dbInsertion():
	persistentCacheDAOImplObj = PersistentCacheDAOImpl()

	key = 10
	value = "test"

	persistentCacheDAOImplObj.insertKeyValue(key, value)
	valueFromDb = (persistentCacheDAOImplObj.getValueByKey(key))["value"]

	assert value == valueFromDb, "db insertion success"