from controllers.cacheControllers.CacheDAOImpl import CacheDAOImpl

def test_cacheInsertion():
	cacheDAOImplObj = CacheDAOImpl()

	key = 10
	value = "test"
	
	cacheDAOImplObj.writeInCache(key, value)
	valueFromCache = cacheDAOImplObj.readFromCache(key)

	assert value == valueFromCache, "cache insertion success"