from libraries.loggerLibrary import LoggerLibrary

class Logger:

    def __init__(self):
        self.loggerObj = LoggerLibrary().getLoggerObj()
    
    def logInfoMessage(self, logMessage):
        self.loggerObj.info(logMessage)

    def logWarningMessage(self, logMessage):
        self.loggerObj.warning(logMessage)

    def logErrorMessage(self, logMessage):
        self.loggerObj.error(logMessage)