from libraries.envLibrary import EnvLibrary
from loaders import loader

class EnvHandler:

    def __init__(self):
        EnvLibrary()
    
    def getEnvValue(self, env):
        return loader.osLibraryObj.getOsObj().getenv(env)