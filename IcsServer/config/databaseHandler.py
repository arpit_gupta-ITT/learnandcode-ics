from libraries.dbConnectLibrary import DbConnectLibrary
from loaders import loader
from constants import constants

class DatabaseHandler:
    dbCollection = None

    def __init__(self):
        self.dbConnectObj = DbConnectLibrary().getDbConnectObj()
        self.connectWithDatabase()
    
    def connectWithDatabase(self):
        databaseObj = self.dbConnectObj.MongoClient(loader.envhandlerObj.getEnvValue("DB_CONNECTION_STRING"))
        collections = databaseObj[constants.DB_NAME]
        self.dbCollection = collections[constants.DB_COLLECTION_NAME]