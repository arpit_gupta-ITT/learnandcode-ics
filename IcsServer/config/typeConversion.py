class TypeConversion:

    def getDataInStringType(self, data):
        return str(data)
    
    def getDataInIntegerType(self, data):
        return int(data)

    def getDataInFloatType(self, data):
        return float(data)