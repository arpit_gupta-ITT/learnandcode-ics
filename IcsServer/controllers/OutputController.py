from loaders import loader

class OutputController:
    
    def printMessageToConsole(self, message):
        print("=> " + message)
    
    def printInfoMessage(self):
        hostName = loader.envhandlerObj.getEnvValue("HOST")
        port = loader.envhandlerObj.getEnvValue("PORT")

        print()
        loader.outputControllerObj.printMessageToConsole("Command to connect with server : ics connect -h " + hostName + " -p " + port)
        loader.outputControllerObj.printMessageToConsole("Command to SET key value : ics SET <key> <value>")
        loader.outputControllerObj.printMessageToConsole("Command to GET value : ics GET <key>")
        print()