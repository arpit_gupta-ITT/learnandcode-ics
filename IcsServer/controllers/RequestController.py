from distutils.log import error
from loaders import loader
from controllers.ThreadManager import ThreadManager

class RequestController:
    
    def __init__(self):    
        self.threadManagerObj = ThreadManager()
    
    def listenRequest(self, socketObj):
        try:
            while True:
                client = self.getClient(socketObj)
                if (self.isValidClient(client)):
                    loader.responseControllerObj.sendResponse(client, "Connected")
                    loader.outputControllerObj.printMessageToConsole("Client successfully connected.")
                    loader.loggerObj.logInfoMessage("Client successfully connected.")
                    self.processRequest(client)
                else:
                    loader.responseControllerObj.sendResponse(client, "Connection failed")
                    loader.loggerObj.logErrorMessage("Connection failed")
        except Exception:
            loader.loggerObj.logErrorMessage("Error occured while listening request.")


    def isValidClient(self, client):
        command = self.receiveData(client)
        isValidConnectionCommand = loader.validationObj.isValidConnectionCommand(command)

        if (isValidConnectionCommand and self.isValidConnection(command)):
            return True
        else:
            return False

    def getClient(self, socketObj):
        client, address = socketObj.accept()
        return client
        
    def processRequest(self, client):
        self.threadManagerObj.createNewThread(client)

    def receiveData(self, client):
        receivedData = client.recv(2048)
        return loader.icsProtocolObj.getDecodedMessage(receivedData)

    def getHostname(self, command):
        initialIndex = 0
        finalIndex = 0
        for index in range(len(command)):
            if (command[index] == "h"):
                initialIndex = index + 1
            if (command[index] == "p"):
                finalIndex = index - 1

        return command[initialIndex : finalIndex].strip()

    def getPort(self, command):
        initialIndex = 0
        for index in range(len(command)):
            if (command[index] == "p"):
                initialIndex = index + 1

        return command[initialIndex : len(command)].strip()

    def isValidConnection(self, command):
        receivedHostname = self.getHostname(command)
        receivedPort = self.getPort(command)
        
        hostname = loader.typeConversionObj.getDataInStringType(loader.envhandlerObj.getEnvValue("HOST")).strip()
        port = loader.typeConversionObj.getDataInStringType(loader.envhandlerObj.getEnvValue("PORT")).strip()
        
        return (receivedHostname == hostname)  and  (receivedPort == port)