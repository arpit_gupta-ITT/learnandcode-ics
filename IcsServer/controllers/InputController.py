from loaders import loader
from services.InputService import InputService

class InputController:
    
    def __init__(self):
        self.inputServiceObj = InputService()

    def handleInput(self, client):
        try:
            while True:
                command = self.getCommand(client)
                isValidCommand = loader.validationObj.isValidCommand(command)

                if ((not command) or (not isValidCommand)):
                    loader.responseControllerObj.sendResponse(client, "Invalid Input")
                    loader.loggerObj.logErrorMessage("Invalid input by client")

                loader.loggerObj.logInfoMessage("Valid input by client")
                self.inputServiceObj.handleCommand(client, command)
        except Exception:
            loader.loggerObj.logErrorMessage("Error occured in handling input")

    def getCommand(self, client):
        return loader.requestControllerObj.receiveData(client)