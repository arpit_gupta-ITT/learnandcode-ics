from loaders import loader
from controllers.SocketManager import SocketManager
from exceptions.ConnectionException import ConnectionException

class ServerController:
    
    def __init__(self):
        self.socketManagerObj = SocketManager()
        self.socketObj = self.socketManagerObj.getSocketObj()

    def startServer(self):
        self.socketObj.bind((loader.icsProtocolObj.getHost(), loader.icsProtocolObj.getPort()))
        self.socketObj.listen()
        loader.outputControllerObj.printMessageToConsole("Server successfully started.")
        loader.outputControllerObj.printInfoMessage()
        loader.loggerObj.logInfoMessage("Server successfully started.")

    def handleRequest(self):
        try:
            loader.requestControllerObj.listenRequest(self.socketObj)
        except ConnectionException:
            loader.loggerObj.logErrorMessage("Error in handling request.")