from loaders import loader
from exceptions.MessageParserException import MessageParserException

class IcsProtocol:
        
    def getHost(self):
        return loader.envhandlerObj.getEnvValue("HOST")

    def getPort(self):
        return int(loader.envhandlerObj.getEnvValue("PORT"))

    def getDecodedMessage(self, value):
        try:
            return value.decode("utf-8")

        except MessageParserException:
            loader.loggerObj.logErrorMessage("Error in message parsing.")

    def getEncodedMessage(self, value):
        try:
            return str.encode(value)
            
        except MessageParserException:
            loader.loggerObj.logErrorMessage("Error in message parsing.")