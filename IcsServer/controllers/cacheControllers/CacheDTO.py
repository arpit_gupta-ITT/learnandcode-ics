class CacheDTO():

    key = 0 
    value = 0
    timeStamp = 0
    
    def __init__(self, key=0, value=0, timeStamp=0):
        self.key = key
        self.value = value
        self.timeStamp = timeStamp
    
    def getKey(self):
        return self.key

    def setKey(self, key):
        self.key = key

    def getValue(self):
        return self.value

    def setValue(self, value):
        self.value = value

    def getTimeStamp(self):
        return self.timeStamp

    def setTimeStamp(self, timeStamp):
        self.timeStamp = timeStamp