from controllers.cacheControllers.CacheDAO import CacheDAO
from controllers.cacheControllers.CacheDTO import CacheDTO
from constants import constants
from loaders import loader
from controllers.cacheControllers.CacheSizeController import CacheSizeController
from controllers.cacheControllers.CacheExpirationTimeController import CacheExpirationTimeController
from exceptions.CacheExceptions.CacheReaderException import CacheReaderException
from exceptions.CacheExceptions.CacheWriterException import CacheWriterException

class CacheDAOImpl(CacheDAO):

    def __init__(self):
        self.cacheExpirationTimeControllerObj = CacheExpirationTimeController()
        self.cacheSizeControllerObj = CacheSizeController()
        self.cache = {}
        self.cacheSize = self.getCacheSize()
        self.head = CacheDTO()
        self.tail = CacheDTO()
        self.head.next = self.tail
        self.tail.pre = self.head

    def writeInCache(self, key, value):
        try:
            self.removeExpiredKeys()
            self.putNode(key, value)
        
        except CacheWriterException:
            loader.loggerObj.logErrorMessage("Error while writing to cache.")

    def readFromCache(self, key):
        try:
            self.removeExpiredKeys()
            return self.getNode(str(key))
        
        except CacheReaderException:
            loader.loggerObj.logErrorMessage("Error while reading from cache.")

    def getNode(self, key):
        node = self.cache.get(key, None)
        if not node:
            return -1
        node = self.getNodeWithTimeStamp(node)
        self.moveNodeToHead(node)
        return node.value

    def moveNodeToHead(self, node):
        self.removeNode(node)
        self.addNode(node)

    def addNode(self, node):
        node.next = self.head.next
        node.pre = self.head
        self.head.next.pre = node
        self.head.next = node
    
    def removeNode(self, node):
        # removeKeyFromCacheBackupFile(node.key)
        pre = node.pre
        new = node.next
        pre.next = new
        new.pre = pre

    def putNode(self, key, value):
        node = self.cache.get(key)
        if not node:             
            self.removeLruNode()
            newNode = CacheDTO()
            newNode.setKey(key)
            newNode.setValue(value)
            newNode = self.getNodeWithTimeStamp(newNode)
            self.cache[key] = newNode
            self.addNode(newNode)
            # addKeyToCacheBackupFile(key)
        else:
            node.value = value
            node = self.getNodeWithTimeStamp(node)
            self.moveNodeToHead(node)

    def removeNodeFromTail(self):
        lruNode = self.tail.pre
        self.removeNode(lruNode)
        return lruNode

    def getExpiredKeys(self):
        expiredKeys = []
        for key in self.cache:
            node = self.cache[key]
            timeStamp = node.timeStamp
            if (self.isKeyExpired(timeStamp)):
                expiredKeys.append(key)
                
        return expiredKeys
    
    def isKeyExpired(self, timeStamp):
        currentTime = self.cacheExpirationTimeControllerObj.getCurrentTime()
        expirationTime = self.cacheExpirationTimeControllerObj.getExpirationTime(timeStamp)
        if (expirationTime > currentTime):
            return False
        else:
            return True

    def removeExpiredKeys(self):
        expiredKeys = self.getExpiredKeys()
        for key in expiredKeys:
            node = self.cache[key]
            self.removeNode(node)
            del self.cache[key]
            self.cacheSize -= 1

    def getCacheSize(self):
        if (constants.CACHE_SIZE_OPTION == constants.CACHE_MANUAL_SIZE_OPTION):
            return 0

        if (constants.CACHE_SIZE_OPTION == constants.CACHE_AUTOMATIC_SIZE_OPTION):
            return self.cacheSizeControllerObj.getAvailablePercentageRAM()

    def getNodeWithTimeStamp(self, node):
        node.timeStamp = self.cacheExpirationTimeControllerObj.getCurrentTime()
        return node
    
    def removeLruNode(self):
        if (constants.CACHE_SIZE_OPTION == constants.CACHE_MANUAL_SIZE_OPTION):
            self.cacheSize += 1
            if self.cacheSize > int(self.cacheSizeControllerObj.getCacheSize()):
                tail = self.removeNodeFromTail()
                del self.cache[tail.key]
                self.cacheSize -= 1

        if (constants.CACHE_SIZE_OPTION == constants.CACHE_AUTOMATIC_SIZE_OPTION):
            if self.cacheSize < int(self.cacheSizeControllerObj.getCacheSize()):
                tail = self.removeNodeFromTail()
                del self.cache[tail.key]