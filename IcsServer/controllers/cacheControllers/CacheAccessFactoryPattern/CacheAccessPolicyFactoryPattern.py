from controllers.cacheControllers.CacheAccessFactoryPattern.CacheWriteBackPolicy import CacheWriteBackPolicy
from controllers.cacheControllers.CacheAccessFactoryPattern.CacheWriteAroundPolicy import CacheWriteAroundPolicy
from controllers.cacheControllers.CacheAccessFactoryPattern.CacheWriteThroughPolicy import CacheWriteThroughPolicy 

class CacheAccessPolicyFactoryPattern:
    
    def getCacheWritePolicy(self, writeType):
        if (writeType == "WRITE_BACK"):
            return CacheWriteBackPolicy()

        elif (writeType == "WRITE_THROUGH"):
            return CacheWriteAroundPolicy()
            
        elif (writeType == "WRITE_AROUND"):
            return CacheWriteThroughPolicy()