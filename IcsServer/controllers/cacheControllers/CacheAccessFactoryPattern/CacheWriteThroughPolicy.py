from controllers.databaseControllers.PersistentCacheDAOImpl import PersistentCacheDAOImpl

class CacheWriteThroughPolicy:
    def __init__(self):
        self.persistentCacheDAOImplObj = PersistentCacheDAOImpl()

    def performWriteOperation(self, cacheObj, key, value):
        self.persistentCacheDAOImplObj.insertKeyValue(key, value)
        cacheObj.writeInCache(key, value)