from controllers.databaseControllers.PersistentCacheDAOImpl import PersistentCacheDAOImpl

class CacheWriteBackPolicy:
    def __init__(self):
        self.persistentCacheDAOImplObj = PersistentCacheDAOImpl()

    def performWriteOperation(self, cacheObj, key, value):
        cacheObj.writeInCache(key, value)
        self.persistentCacheDAOImplObj.insertKeyValue(key, value)