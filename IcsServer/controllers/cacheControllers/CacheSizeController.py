from constants import constants
import psutil

class CacheSizeController:
    
    def getCacheSize(self):
        if (str(constants.CACHE_SIZE_OPTION) == "MANUAL"):
            return int(constants.CACHE_MANUAL_SIZE)

        if (str(constants.CACHE_SIZE_OPTION) == "AUTOMATIC"):
            return int(constants.CACHE_AUTOMATIC_SIZE_PERCENTAGE_RAM)
    
    def getAvailablePercentageRAM(self):
        usedPercentageRAM = psutil.virtual_memory()[2]
        return int(100 - usedPercentageRAM)