from datetime import timedelta, datetime
from constants import constants

class CacheExpirationTimeController:
    
    def getCurrentTime(self):
        return datetime.now()

    def getTimeToLive(self):
        return constants.CACHE_EXPIRATION_TIME_IN_SECONDS

    def getExpirationTime(self, time):
        timeToLive = self.getTimeToLive()
        return time + timedelta(seconds = timeToLive)