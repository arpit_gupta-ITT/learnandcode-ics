from abc import ABC as AbstractBaseClass
from abc import abstractmethod

class CacheDAO(AbstractBaseClass):

    @abstractmethod
    def writeInCache(self, key, value):
        pass

    @abstractmethod
    def readFromCache(self, key):
        pass