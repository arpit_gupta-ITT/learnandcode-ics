from libraries.threadLibrary import ThreadLibrary
from controllers.InputController import InputController
from loaders import loader

class ThreadManager:
    threadCount = 0
    
    def __init__(self):    
        self.threadLibraryObj = ThreadLibrary()
        self.inputControllerObj = InputController()
    
    def createNewThread(self, client):
        newThread = self.threadLibraryObj.getThreadObj()
        newThread(self.handleThread, (client,))
        self.threadCount += 1
        loader.outputControllerObj.printMessageToConsole(
            "Client is handled with Thread Number : " + loader.typeConversionObj.getDataInStringType(self.threadCount)
        )
        loader.loggerObj.logInfoMessage("Client is handled with Thread Number : " + loader.typeConversionObj.getDataInStringType(self.threadCount))
    
    def handleThread(self, client):
        self.inputControllerObj.handleInput(client)