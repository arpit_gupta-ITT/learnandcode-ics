from loaders import loader

class ResponseController:

    def sendResponse(self, client, data):
        client.send(loader.icsProtocolObj.getEncodedMessage(data))