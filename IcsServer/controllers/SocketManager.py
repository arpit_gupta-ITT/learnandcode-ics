from libraries.socketLibrary import SocketLibrary

class SocketManager:
    
    def __init__(self):
        self.socketObj = SocketLibrary().getSocket()
    
    def getSocketObj(self):
        return self.socketObj
    
    def closeSocket(self):
        self.socketObj.close()