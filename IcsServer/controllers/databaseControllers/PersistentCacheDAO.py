from abc import ABC as AbstractBaseClass
from abc import abstractmethod

class PersistentCacheDAO(AbstractBaseClass):

    @abstractmethod
    def insertKeyValue(self, key, value):
        pass

    @abstractmethod
    def getValueByKey(self, key):
        pass

    @abstractmethod
    def getValuesByKeys(self, keys):
        pass