from controllers.databaseControllers.PersistentCacheDAO import PersistentCacheDAO
from config.databaseHandler import DatabaseHandler
from exceptions.DatabaseExceptions.DatabaseReaderException import DatabaseReaderException
from exceptions.DatabaseExceptions.DatabaseWriterException import DatabaseWriterException 
from loaders import loader

class PersistentCacheDAOImpl(PersistentCacheDAO):

    def __init__(self):
        self.databaseHandlerObj = DatabaseHandler()

    def insertKeyValue(self, key, value):
        try:
            if (self.isKeyExist(key)):
                self.databaseHandlerObj.dbCollection.find_one_and_update(
                    { "key" : key },
                    { "$set": { "value": value } },
                    upsert = True
                )
            else:
                payload = { "key": key, "value": value }
                self.databaseHandlerObj.dbCollection.insert_one(payload)

        except DatabaseWriterException:
            loader.loggerObj.logErrorMessage("Error in writing to db.")
            
    def getValueByKey(self, key):
        try:
            return self.databaseHandlerObj.dbCollection.find_one({ 'key' : key }, { "_id":0, "key": 1, "value": 1 })
        
        except DatabaseReaderException:
            loader.loggerObj.logErrorMessage("Error in reading from db.")

    def getValuesByKeys(self, keys):
        keyValues = []
        for key in keys:
            keyValues.append(self.getValueByKey(key))

        return keyValues

    def isKeyExist(self, key):
        return bool(self.databaseHandlerObj.dbCollection.find_one({ 'key' : key }))