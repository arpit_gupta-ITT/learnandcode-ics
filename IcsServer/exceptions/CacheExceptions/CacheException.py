from exceptions.ICSException import ICSException

class CacheException(ICSException):

	def __init__(self, message = "Error in cache operation."):
		self.message = message