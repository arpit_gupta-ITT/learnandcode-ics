from exceptions.CacheExceptions.CacheException import CacheException

class CacheReaderException(CacheException):

	def __init__(self, message = "Error while reading from cache."):
		self.message = message