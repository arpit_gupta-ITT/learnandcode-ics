from exceptions.CacheExceptions.CacheException import CacheException

class CacheWriterException(CacheException):

	def __init__(self, message = "Error in writing to cache."):
		self.message = message