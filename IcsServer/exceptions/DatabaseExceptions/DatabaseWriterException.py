from exceptions.DatabaseExceptions.DatabaseException import DatabaseException

class DatabaseWriterException(DatabaseException):

	def __init__(self, message = "Error in writing to database."):
		self.message = message