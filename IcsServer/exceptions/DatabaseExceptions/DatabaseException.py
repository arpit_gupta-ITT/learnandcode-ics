from exceptions.ICSException import ICSException

class DatabaseException(ICSException):

	def __init__(self, message = "Error in database connection."):
		self.message = message