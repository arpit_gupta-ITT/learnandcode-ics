from exceptions.DatabaseExceptions.DatabaseException import DatabaseException

class DatabaseReaderException(DatabaseException):

	def __init__(self, message = "Error while reading from database."):
		self.message = message