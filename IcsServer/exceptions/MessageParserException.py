from exceptions.ICSException import ICSException

class MessageParserException(ICSException):

	def __init__(self, message = "Error in message parsing."):
		self.message = message