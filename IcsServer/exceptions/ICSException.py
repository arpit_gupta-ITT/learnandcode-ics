class ICSException(Exception):
	def __init__(self, message = "Error while reading from cache."):
		self.message = message
		super().__init__(self.message)