from exceptions.ICSException import ICSException

class ConnectionException(ICSException):

	def __init__(self, message = "Unable to establish connection."):
		self.message = message