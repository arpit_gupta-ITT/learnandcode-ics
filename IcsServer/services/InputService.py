from constants import constants
from controllers.cacheControllers.CacheAccessFactoryPattern.CacheAccessPolicyFactoryPattern import CacheAccessPolicyFactoryPattern
from controllers.cacheControllers.CacheDAOImpl import CacheDAOImpl
from controllers.databaseControllers.PersistentCacheDAOImpl import PersistentCacheDAOImpl
from loaders import loader

class InputService:
    
    def __init__(self):    
        self.cacheAccessPolicyFactoryPatternObj = CacheAccessPolicyFactoryPattern()
        self.cacheDAOImplObj = CacheDAOImpl()
        self.persistentCacheDAOImplObj = PersistentCacheDAOImpl()

    def handleCommand(self, client, command):
        if (loader.validationObj.isValidGetCommand(command)):
            self.handleGetCommand(client, command)
            loader.loggerObj.logInfoMessage("Client entered get command")

        elif (loader.validationObj.isValidSetCommand(command)):
            self.handleSetCommand(client, command)
            loader.loggerObj.logInfoMessage("Client entered set command")

    def handleGetCommand(self, client, command):
        key = self.getKey(command)
        value = self.cacheDAOImplObj.readFromCache(key)          
        loader.loggerObj.logInfoMessage("Value fetched from cache.")
        
        if (value == -1):
            value = (self.persistentCacheDAOImplObj.getValueByKey(key))["value"]
            loader.loggerObj.logInfoMessage("Value fetched from db.")

            if (not value):
                loader.loggerObj.logErrorMessage("Value not found in db.")
                return loader.responseControllerObj.sendResponse(client, "Value not found")
    
            loader.responseControllerObj.sendResponse(client, value)
            self.writeToCache(key, value)
        else:
            loader.responseControllerObj.sendResponse(client, value)
    
    def handleSetCommand(self, client, command):
        command = command.split("SET")
        keyValue = command[1].strip()
        keyValue = keyValue.split()
        key = keyValue[0].strip()
        value = keyValue[1].strip()

        self.writeToCache(key, value)
    
    def getKey(self, command):
        command = command.split("GET")
        return command[1].strip()

    def writeToCache(self, key, value):
        cacheWritePolicy = self.cacheAccessPolicyFactoryPatternObj.getCacheWritePolicy(constants.CACHE_ACCESS_POLICY)
        cacheWritePolicy.performWriteOperation(self.cacheDAOImplObj, key, value)
        loader.loggerObj.logInfoMessage("Value added to cache.")
