from loaders import loader
import re
from constants import constants
 
class RequestController:
    
    def sendRequest(self, isConnected, socketObj):
        if (isConnected):
            loader.outputControllerObj.printMessageToConsole("Successfully connected to server.")
            self.handleRequest(socketObj)
        else:
            loader.outputControllerObj.printMessageToConsole("Connection Failed.")

    def handleRequest(self, socketObj):
        while True:
            command = loader.inputControllerObj.getInput("Enter command to GET or SET values : ")
            isValidGetCommand = self.isValidGetCommand(command)
            isValidSetCommand = self.isValidSetCommand(command)
            
            if (isValidGetCommand):
                loader.outputControllerObj.sendData(socketObj, command)
                value = loader.inputControllerObj.receiveData(socketObj)
                loader.outputControllerObj.printMessageToConsole("Value : " + value)
            
            if (isValidSetCommand):
                loader.outputControllerObj.sendData(socketObj, command)

    def isValidGetCommand(self, command):
        return re.match(constants.GET_COMMAND_PATTERN, command)
    
    def isValidSetCommand(self, command):
        return re.match(constants.SET_COMMAND_PATTERN, command)