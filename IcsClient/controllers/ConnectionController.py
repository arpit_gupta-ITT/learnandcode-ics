from loaders import loader
import socket

class ConnectionController:

    def connectWithServer(self):
        try:
            socketObj = socket.socket()
            loader.outputControllerObj.printInfoMessage()
            commandToConnect = loader.inputControllerObj.getInput("Enter command to connect with server : ")
            socketObj.connect((self.getHostname(commandToConnect), int(self.getPort(commandToConnect))))
            loader.outputControllerObj.sendData(socketObj, commandToConnect)
            response = loader.inputControllerObj.receiveData(socketObj)
            
            if (response == "Connected"):
                return True, socketObj
            else:
                return False, socketObj

        except socket.error as e:
            socketObj.close()
            print(str(e))


    def getHostname(self, command):
        initialIndex = 0
        finalIndex = 0
        for index in range(len(command)):
            if (command[index] == "h"):
                initialIndex = index + 1
            if (command[index] == "p"):
                finalIndex = index - 1

        return command[initialIndex : finalIndex].strip()

    def getPort(self, command):
        initialIndex = 0
        for index in range(len(command)):
            if (command[index] == "p"):
                initialIndex = index + 1

        return command[initialIndex : len(command)].strip()