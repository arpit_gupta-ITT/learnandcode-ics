class InputController:
    
    def getInput(self, message):
        return str(input(message))

    def receiveData(self, socketObj):
        return socketObj.recv(1024).decode('utf-8')