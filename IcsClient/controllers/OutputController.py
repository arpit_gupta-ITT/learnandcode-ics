from loaders import loader

class OutputController:
    
    def printMessageToConsole(self, message):
        print("=> " + message)

    def sendData(self, socketObj, command):
        socketObj.send(str.encode(command))

    def printInfoMessage(self):
        print()
        loader.outputControllerObj.printMessageToConsole("Command to connect with server : ics connect -h <host> -p <port>")
        loader.outputControllerObj.printMessageToConsole("Command to SET key value : ics SET <key> <value>")
        loader.outputControllerObj.printMessageToConsole("Command to GET value : ics GET <key>")
        print()