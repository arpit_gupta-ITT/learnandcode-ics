from controllers.ConnectionController import ConnectionController
from controllers.RequestController import RequestController

connectionControllerObj = ConnectionController()
isConnected, socketObj = connectionControllerObj.connectWithServer()
requestControllerObj = RequestController()
requestControllerObj.sendRequest(isConnected, socketObj)