GET_COMMAND_PATTERN = "^ics +GET +[0-9a-zA-Z]+ *$"
SET_COMMAND_PATTERN = "^ics +SET +[0-9a-zA-Z]+ +[0-9a-zA-Z]+ *$"
CONNECTION_COMMAND_PATTERN = "^ics +connect +-h +(((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\.){3}(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])) +-p +([0-9]{3,4}) *$"
KEY_VALUE_PATTERN = "^[a-zA-Z0-9]+$"